<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use softDeletes;

    protected $fillable = ['title', 'description', 'status', 'user_id', 'todo_id'];

    protected $dates = ['delete_at'];

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'task_user', 'task_id', 'user_id');
    }

    public function todo()
    {
        return $this->belongsTo(Todo::class);
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'commentable');
    }
}
