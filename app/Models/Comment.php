<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Comment extends Model
{
    protected $fillable = ['comment', 'datetime', 'user_id', 'todo_id', 'task_id'];

    protected $with = ['user'];

    public function  user()
    {
        return $this->belongsTo(User::class);
    }

    public function commentable()
    {
        return $this->morphTo();
    }
}
