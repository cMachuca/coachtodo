<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'comment' => $this->comment,
            'datetime' => $this->datetime,
            'user' => new User($this->whenLoaded('user')),
            //'todo_id' => new Todo($this->todo),
            //'task_id' => new TaskRequest($this->tasks),
        ];
    }
}
