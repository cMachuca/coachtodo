<?php

namespace App\Http\Resources;

use App\Http\Resources\User;
use Illuminate\Http\Resources\Json\JsonResource;

class Todo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'target_date' => $this->target_date,
            'user' => User::collection($this->user),
            //'tasks' => Todo::collection($this->tasks),
            //'comments' => Todo::collection($this->comments),
        ];
    }
}
