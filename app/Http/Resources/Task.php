<?php

namespace App\Http\Resources;

//use App\Http\Resources\User;
//use App\Http\Resources\Todo;
//use App\Http\Resources\CommentRequest;
use Illuminate\Http\Resources\Json\JsonResource;

class Task extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'status' => $this->status,
            'user' => new User($this->whenLoaded('user')),
            'todo' => new Todo($this->whenLoaded('todo')),
            'comments' => Comment::collection($this->comments),
        ];
    }
}
