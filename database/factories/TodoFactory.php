<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Todo::class, function (Faker $faker) {
    return [
        'title'=> $faker->sentence($nbWords = 6, $variableNbWords = true),
        'description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'target_date' => $faker->dateTime($max = 'now', $timezone = null),
        'user_id' => $faker->numberBetween($min = 1, $max = 50),
    ];
});
