<?php

use Faker\Generator as Faker;

$factory->define(App\Models\TaskUser::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween($min = 1, $max = 50),
        'task_id' => $faker->numberBetween($min = 1, $max = 80),
    ];
});
