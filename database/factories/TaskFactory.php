<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Task::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'user_id' => $faker->numberBetween($min = 1, $max = 50),
        'todo_id' => $faker->numberBetween($min = 1, $max = 10),
    ];
});
