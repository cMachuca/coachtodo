<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    $morphs = ['App\Models\Task', 'App\Models\Todo'];

    return [
        'comment' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'datetime' => $faker->dateTime($max = 'now', $timezone = null),
        'user_id' => $faker->numberBetween($min = 1, $max = 50),
        'commentable_id' => $faker->numberBetween($min = 1, $max = 80),
        'commentable_type' => $morphs[$faker->numberBetween($min = 0, $max = 1)]
    ];
});
