<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');

Route::group(['prefix' => 'json/v1', 'middleware' => ['jwt.auth']], function() {
    Route::resource('users', 'UserController')->except(['create', 'edit', 'store']);
    // route to get todos for one user
    Route::get('users/{id}/todos', 'UserController@todos');
    Route::get('users/{id}/tasks', 'UserController@tasks');
    Route::resource('todos', 'TodoController')->except(['create', 'edit']);

    Route::resource('tasks', 'TaskController')->except(['create', 'edit']);
    Route::resource('comments', 'CommentController')->except(['create', 'edit']);
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
